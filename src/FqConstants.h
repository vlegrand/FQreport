/*

  Copyright (C) 2016-2021  Institut Pasteur
 
  This program is part of the FQReport software.
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Veronique Legrand                                                           veronique.legrand@pasteur.fr
 
 */

#ifndef FQCONSTANTS_H_
#define FQCONSTANTS_H_

#define k_read_id_start '@'
#define k_read_qual_start '+'
#define k_phred_33 33
#define k_phred_64 64
#define k_max_score 126
#define MAX_PHRED_SCORE 93 //! maximum phred score for a nucleotide in a fastq file is given by char "~" (ascii code 126). So maximum phred score for a nucleotide is 126-32=93

//! for all possible nucleotide phred score, store here the coresponding error probability.
static const double k_exp_err_probas[61]={1.0000000000000000,0.7943282347242815,0.6309573444801932,\
								   0.5011872336272722,0.3981071705534972,0.3162277660168379,\
								   0.2511886431509580,0.1995262314968880,0.1584893192461113,\
								   0.1258925411794167,0.1000000000000000,0.0794328234724281,\
								   0.0630957344480193,0.0501187233627272,0.0398107170553497,\
								   0.0316227766016838,0.0251188643150958,0.0199526231496888,\
								   0.0158489319246111,0.0125892541179417,0.0100000000000000,\
								   0.0079432823472428,0.0063095734448019,0.0050118723362727,\
								   0.0039810717055350,0.0031622776601684,0.0025118864315096,\
								   0.0019952623149689,0.0015848931924611,0.0012589254117942,\
								   0.0010000000000000,0.0007943282347243,0.0006309573444802,\
								   0.0005011872336273,0.0003981071705535,0.0003162277660168,\
								   0.0002511886431510,0.0001995262314969,0.0001584893192461,\
								   0.0001258925411794,0.0001000000000000,0.0000794328234724,\
								   0.0000630957344480,0.0000501187233627,0.0000398107170553,\
								   0.0000316227766017,0.0000251188643151,0.0000199526231497,\
								   0.0000158489319246,0.0000125892541179,0.0000100000000000,\
								   0.0000079432823472,0.0000063095734448,0.0000050118723363,\
								   0.0000039810717055,0.0000031622776602,0.0000025118864315,\
								   0.0000019952623150,0.0000015848931925,0.0000012589254118,\
								   0.0000010000000000};

static const char all_poss_score_32[127]={0,0,0,0,0,0,0,0,0,0,0,\
								   	   	  0,0,0,0,0,0,0,0,0,0,\
										  0,0,0,0,0,0,0,0,0,0,\
										  0,0,0,1,2,3,4,5,6,7,\
										  8,9,10,11,12,13,14,15,16,17,\
										  18,19,20,21,22,23,24,25,26,27,\
										  28,29,30,31,32,33,34,35,36,37,\
										  38,39,40,41,42,43,44,45,46,47,\
										  48,49,50,51,52,53,54,55,56,57,\
										  58,59,60,61,62,63,64,65,66,67,\
										  68,69,70,71,72,73,74,75,76,77,\
										  78,79,80,81,82,83,84,85,86,87,\
										  88,89,90,91,92,93};


static const char all_poss_score_64[127]={0,0,0,0,0,0,0,0,0,0,0,\
								   	   	  0,0,0,0,0,0,0,0,0,0,\
										  0,0,0,0,0,0,0,0,0,0,\
										  0,0,0,0,0,0,0,0,0,0,\
										  0,0,0,0,0,0,0,0,0,0,\
										  0,0,0,0,0,0,0,0,0,0,\
										  0,0,0,1,2,3,4,5,6,7,\
										  8,9,10,11,12,13,14,15,16,17,\
										  18,19,20,21,22,23,24,25,26,27,\
										  28,29,30,31,32,33,34,35,36,37,\
										  38,39,40,41,42,43,44,45,46,47,\
										  48,49,50,51,52,53,54,55,56,57,\
										  58,59,60,61,62,63};

#define MAX_READ_LENGTH 1024 // TODO This will likely become a parameter of the program later. set this arbitrary for the moment.

#define MAX_FQ_RECORD_LENGTH 3000 // TODO: There could be long reads with length >=100000 bases. Take that into account.

#endif /* FQCONSTANTS_H_ */
