/*
 * InfoDiplay.h
 *
 *  Created on: Nov 2, 2021
 *      Author: vlegrand
 */

#ifndef SRC_INFODISPLAY_H_
#define SRC_INFODISPLAY_H_

#include <iostream>
#include <iomanip>

#include "FqInfos.h"


class InfoDisplay {


public:
	ostream * out;

	InfoDisplay(ostream* out) {
		this->out=out;
	}

	void display(const FqInfos& infos);

private:
	void display0(unsigned int);
	void displayPosition(unsigned int i);
	void displayFloat2D(double num,unsigned int);
	void displayQuartiles(const FqInfos& infos,unsigned int i);
	void displayNuclPosScorePC(const FqInfos& infos,unsigned int i);
	void displayAllPosLines(const FqInfos& infos);
	void displayFload1DLine(const FqInfos& infos,const T_array_PhScore& r);
};





#endif /* SRC_INFODISPLAY_H_ */
