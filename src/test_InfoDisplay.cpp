/*
 * test_infoDisplay.cpp
 *
 *  Created on: Nov 8, 2021
 *      Author: vlegrand
 */
#include <fstream>
#include <iostream>
#include "catch.hpp"
#include "FqInfos.h"
#include "InfoDisplay.h"



int compareFilesLileByLine(char * filename1,char* filename2) {
    ifstream file1,file2;
    file1.open(filename1,ios::binary|ios::in);
    assert(file1.good());
    file2.open(filename2,ios::binary|ios::in);
    assert(file2.good());
//---------- compare number of lines in both files ------------------
    int c1,c2;
    c1 = 0; c2 = 0;
    string str,string1,string2;
    while(!file1.eof())
    {
        getline(file1,str);
        c1++;
    }
    file1.clear();   //  sets a new value for the error control state
    file1.seekg(0,ios::beg);
    while(!file2.eof())
    {
        getline(file2,str);
        c2++;
    }
    file2.clear();
    file2.seekg(0,ios::beg);

    if(c1 != c2)
    {
        cout << "Different number of lines in files!" << "\n";
        cout << "file1 has " << c1 << " lines and file2 has "
                     << c2 << " lines" << "\n";
        return -1;
    }
//---------- compare two files line by line ------------------
    //char string1[90000], string2[90000]; // I know that my test data are smaller than that.
    int j = 0;
    while(!file1.eof())
    {
        getline(file1,string1);
        getline(file2,string2);
        j++;
        if(string(string1)!=string(string2))
        {
            cout << j << "-th strings are not equal" << "\n";
            cout << "   " << string1 << "\n";
            cout << "   " << string2 << "\n";
            return -1;
        }
    }

    return 0;
}

TEST_CASE("Loads a serialized FqInfos object and format its content for display") {
	FqInfos fq_infos;
	//fq_infos.deserialize("../../test/data/FqInfos.txt");
	fq_infos.deserialize("FqInfos.txt");
	ofstream o("formatted.txt");
	InfoDisplay d=InfoDisplay(&o);
	d.display(fq_infos);
	//int ret=compareFilesLileByLine((char *) "formatted.txt",(char *) "../../test/data/output_fastq_info.txt");
	int ret=compareFilesLileByLine((char *) "formatted.txt",(char *) "output_fastq_info.txt");
	REQUIRE(ret==0);
}
