/*
 * FqBaseBackend.cpp
 *
 *  Created on: Jan 20, 2016
 *      Author: vlegrand
 */

#include <sys/stat.h>
#include <iostream>
#include <string>
#include <cstring>
#include <stdexcept>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>
#include <math.h>
#include <array>



#include "FqBackend.h"

//#define DEBUG
#ifdef DEBUG
#include <cassert>
#include <iostream>
#endif

using namespace std;

// #define BENCHMARK
#ifdef BENCHMARK
#include <sys/time.h>
#include <sys/resource.h>
void printRUsage() {
    struct rusage usage;
    int res=getrusage(RUSAGE_SELF,&usage);
    std::cout<<"memory info: maxrss="<<usage.ru_maxrss<<" ixrss="<<usage.ru_ixrss<<" idrss="<<usage.ru_idrss<<" isrss="<<usage.ru_isrss<<" minflt="<<usage.ru_minflt<<" majflt="<<usage.ru_majflt<<endl;
    std::cout<<"time info: user time="<<usage.ru_utime.tv_sec<<" system time="<<usage.ru_stime.tv_sec<<endl;
}
#endif


T_buf_info init_buf_info(int& nread,char * buf) {
    T_buf_info buf_info;
    buf_info.real_bufsize=nread;
    buf_info.cnt=0;
    buf_info.pchar=buf;
    buf_info.buf=buf;
    buf_info.p_start_cur_rec=buf;
    return buf_info;
}

void FqBackend::openInputFile() {
    mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;

    closeInputFile();

    i_f_desc=open(fq_infos.filename.c_str(),O_RDONLY,mode);
    if (i_f_desc==-1) {
        err(errno,"cannot open file: %s.",fq_infos.filename.c_str());
    }
}


void FqBackend::closeInputFile() {
    if (i_f_desc!=-1) {
        close(i_f_desc);
        i_f_desc=-1;
    }
}

//TODO: adapt this for phred_32.
/*void FqBackend::compute_all_scores() {
	for (unsigned char i=k_phred_32;i<=k_max_score;i++) {
		all_poss_scores[i]=i-k_phred_32;
	}
}*/

void FqBackend::processFile() {
    int nread;
    char* buf;

    size_t buffer_size;
    (test_mode)?buffer_size=test_bufsize:buffer_size=FqBackend::bufsize;
    //compute_all_scores();
#ifdef BENCHMARK
    cout<<"starting loading file"<<endl;
	printRUsage();
#endif
    openInputFile();
    buf=(char *) malloc(buffer_size*sizeof(char));
    if (buf==NULL) {
        err(errno,"cannot allocate memory: %lu bytes.",buffer_size);
    }
    /* for each read, we wanttotal quality score. */
    while ((nread=read(i_f_desc,buf,buffer_size))!=0) {
        //cur_offset = lseek(i_f_desc, 0, SEEK_CUR);
        T_buf_info buf_info=init_buf_info(nread,buf);
        processBuf(buf_info);
    }
    free(buf);
    closeInputFile();
#ifdef BENCHMARK
    cout<<"finished loading file"<<endl;
	printRUsage();
#endif
    fq_infos.computeAll();
#ifdef BENCHMARK
    cout<<"finished my calculations"<<endl;
	printRUsage();
#endif
}


void FqBackend::processCin() {
	char* buf;
	int nread;
	size_t buffer_size;
	(test_mode)?buffer_size=test_bufsize:buffer_size=FqBackend::bufsize;
	buf=(char *) malloc(buffer_size*sizeof(char));
	if (buf==NULL) {
		err(errno,"cannot allocate memory: %lu bytes.",buffer_size);
	}
	while (cin.get (buf,buffer_size,EOF)) {
		nread=cin.gcount();
		T_buf_info buf_info=init_buf_info(nread,buf);
		processBuf(buf_info);
	}
	free(buf);
	fq_infos.computeAll();
}

inline void FqBackend::onNucl(const char nucl,const unsigned int idx) {
	switch(nucl) {
	case 'A':
		++fq_infos.A_nb[idx];
		break;
	case 'C':
		++fq_infos.C_nb[idx];
		break;
	case 'G':
		++fq_infos.G_nb[idx];
		break;
	case 'T':
		++fq_infos.T_nb[idx];
		break;
	case 'N':
		++fq_infos.N_nb[idx];
        break;
	default:
		break;
	}
}



void FqBackend::processBuf(T_buf_info& buf_info) {
    static T_fq_rec_info fq_rec_info;
    static int num_l_in_rec=5; // counter to know on which line inside the fastq record we are
    int start_rec_in_buf=0;
    while (buf_info.cnt<=buf_info.real_bufsize-1) {
        switch (*buf_info.pchar){
            case k_read_id_start:
            	if (num_l_in_rec==4) goto inc_score;
            	// otherwise, we are at the beginning of a new fastq record.
                buf_info.p_start_cur_rec=buf_info.pchar;
                start_rec_in_buf=buf_info.cnt;
                num_l_in_rec=1;
                fq_rec_info.nb_nucleotides_in_read=0;
                fq_rec_info.sum_pi=0;
                fq_rec_info.idx_nucl_in_read=0;
                fq_rec_info.st=0;
                fq_infos.nb_reads++;
                break;
            case '\n':
                 num_l_in_rec+=1;
                 if (num_l_in_rec==5) onEndFastqRecord(fq_rec_info);
                 break;
            default:
            	if (num_l_in_rec==2) {
            		onNucl(*buf_info.pchar,fq_rec_info.nb_nucleotides_in_read);
            		fq_rec_info.nb_nucleotides_in_read++;
            	}
            	inc_score: {if (num_l_in_rec==4) onIncScore(fq_rec_info,buf_info);
            		}
                break;
        }
        buf_info.pchar++;
        buf_info.cnt++;
    }
    keepCurFastqRecord(buf_info.buf,start_rec_in_buf,buf_info.real_bufsize);
}


/*
void FqBackend::processBuf_old(T_buf_info& buf_info) {
    static T_fq_rec_info fq_rec_info;
    static int num_l_in_rec; //counter to know on which line inside the fastq record we are
    static int qual_score=0;
    int start_rec_in_buf=0;

    while (buf_info.cnt<=buf_info.real_bufsize-1) {
        switch (*buf_info.pchar){
            case k_read_id_start:
                if (qual_score) goto inc_score;
                else {
                    buf_info.p_start_cur_rec=buf_info.pchar;
                    start_rec_in_buf=buf_info.cnt;
                    num_l_in_rec=1;
                    fq_rec_info.nb_nucleotides_in_read=0;
                    fq_rec_info.sum_pi=0;
                    ++fq_infos.nb_reads;
                }
                break;
            case k_read_qual_start:
            	if (qual_score) goto inc_score; // In this case we are already processing the read score. The + char is simply phred score 43-33=10 if phred33.
            	else {
            		qual_score=1;
            		fq_rec_info.st=0;
            		fq_rec_info.idx_nucl_in_read=0;
            	}
                break;
            case '\n':
                (qual_score==1)?qual_score++:qual_score;
                num_l_in_rec+=1;
                if (num_l_in_rec==5) {
                    qual_score=0;// end of fastq record
                    onEndFastqRecord(fq_rec_info);
                 }
                break;
            default:
            if (num_l_in_rec==2) {
            	onNucl(*buf_info.pchar,fq_rec_info.nb_nucleotides_in_read);
            	++fq_rec_info.nb_nucleotides_in_read;
            }
            inc_score:
                { if (qual_score==2) onIncScore(fq_rec_info,buf_info);
                }
                break;
        }
        ++buf_info.pchar;
        ++buf_info.cnt;
    }
    keepCurFastqRecord(buf_info.buf,start_rec_in_buf,buf_info.real_bufsize);
}
*/


void FqBackend::onEndFastqRecord(T_fq_rec_info& rec_info) {
    unsigned int v=floor(rec_info.sum_pi);
    fq_infos.EFreq_nb[v]+=1;
     // empty buffer keeping current record
     cur_fq_record[0]='\0';
     if (rec_info.nb_nucleotides_in_read>fq_infos.real_max_r_len) fq_infos.real_max_r_len=rec_info.nb_nucleotides_in_read;
     ++fq_infos.read_phred_scores[floor(0.5+(double) rec_info.st/rec_info.nb_nucleotides_in_read)];
     fq_infos.LFreq_nb[rec_info.nb_nucleotides_in_read]+=1;
}



/*
 * use C style char* rather than std::string for a matter of performance.
 * use read instead of fread for the same reason.
 *
 * Note: length of the string containing the read is returned.
 */


/* keep the end of current fastq record in memory. Used for records that are incomplete (when reading big files an fq record'start may be read in buffer 1
 whereas its end may be in buffer 2...*/
void FqBackend::keepCurFastqRecord(char * buf,const int& start_rec_in_buf,const int &nread) {
    int len=nread-start_rec_in_buf;
    if (len+1>=MAX_FQ_RECORD_LENGTH) errx(1,"Buffer for current record is too small given record length.");
    memcpy(cur_fq_record,&buf[start_rec_in_buf],len);
    cur_fq_record[len]='\0';
}

void FqBackend::onIncScore(T_fq_rec_info& rec_info,T_buf_info& buf_info) {
	char s;
	(phred==k_phred_33)?s=all_poss_score_32[(unsigned int)*buf_info.pchar]:s=all_poss_score_64[(unsigned int)*buf_info.pchar];
    //char s=all_poss_score_32[(unsigned int)*buf_info.pchar];
    fq_infos.real_max_score<s?fq_infos.real_max_score=s:fq_infos.real_max_score;
    ++fq_infos.nb_bases;
    ++fq_infos.nb_nucl_with_phscore_at_pos[rec_info.idx_nucl_in_read] [s];
    ++fq_infos.phScoreDist_per_pos[s];
    rec_info.st+=s;
    ++rec_info.idx_nucl_in_read;
    rec_info.sum_pi+=k_exp_err_probas[(unsigned int) s];
}

FqInfos FqBackend::getInfos() {
	return(fq_infos);
}




