/*

  Copyright (C) 2016-2021  Institut Pasteur
 
  This program is part of the ROCK software.
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Veronique Legrand                                                           veronique.legrand@pasteur.fr
 
 */



#ifndef FQBASEBACKEND_H_
#define FQBASEBACKEND_H_

#include <string>
#include <cstring>
#include <map>
#include "FqInfos.h"

using namespace std;

/*
 * Auxilliary structure for buffer processing.
 */
typedef struct {
    int cnt; // number of char already processed in buffer
    char * pchar; // point on current char in buffer
    int real_bufsize; // total number of char in buffer
    char * buf; // pointer to start of buffer
    char * p_start_cur_rec; // pointer to the start of the current record.
}T_buf_info;

/*
 * Auxilliary structure for fastq parsing; temporary gather here information on the fastq record that is being processed.
 */
typedef struct {
    unsigned int nb_nucleotides_in_read; // ! number of nucleotides in read
    unsigned int st; // total read score (sum of the nucleotides quality score).
    unsigned int idx_nucl_in_read; //! To be able to keep in memory how many times we find the given score at a given position.
    double sum_pi; // for the calculation of the average error.
}T_fq_rec_info;



T_buf_info init_buf_info(int& nread,char * buf);


class FqBackend {
private:
	unsigned int phred;
	void openInputFile();
	void closeInputFile();
	void onIncScore(T_fq_rec_info& rec_info,T_buf_info& buf_info);
	void keepCurFastqRecord(char * buf,const int& start_rec_in_buf,const int &nread);
	void processBuf(T_buf_info& buf_info);
	//void processBufSimple(T_buf_info& buf_info);
	void onEndFastqRecord(T_fq_rec_info& rec_info);
	void computeAllQuartiles();
	void computeAverageRLen();
	void onNucl(const char nucl,const unsigned int idx);
	void computeACGTpercent();
	void computeACGTpercentAux(vector<double>& X_nb);


protected:
    static const size_t bufsize=6048000;
    int test_mode;
    size_t test_bufsize;

    // handling input
    //char * i_filename;
    int i_f_desc;

    T_buf_info buf_info;
    char cur_fq_record[MAX_FQ_RECORD_LENGTH];

    //results
    FqInfos fq_infos;


public:

    FqBackend(const string& ficname,const string& samplename="",unsigned int phred=k_phred_33) {
    	this->phred=phred;
    	test_mode=0;
    	test_bufsize=0;
    	i_f_desc=-1;
    	strcpy(cur_fq_record,"");
    	fq_infos=FqInfos(ficname,samplename);
    }

    ~FqBackend() {
    }


    void processFile();
    void processCin();

    FqInfos getInfos();
};



#endif /* FQBASEBACKEND_H_ */
