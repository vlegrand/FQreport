/*
 * test_FqBackend.cpp
 *
 *  Created on: Oct 7, 2021
 *      Author: vlegrand
 */
#include <iostream>
#include <fstream>
#include <math.h>
#include "catch.hpp"
#include "FqInfos.h"
#include "FqBackend.h"

typedef struct {
	unsigned long LFreq_nb;
	double EFreq_nb;
	unsigned long fi_quartile_phred;
	unsigned long sec_quartile_phred;
	unsigned long thi_quartile_phred;
	double A_pc;
	double C_pc;
	double G_pc;
	double T_pc;
	double N_pc;
} T_expected;

T_expected exp1{0,6,33,34,34,17.00,28.00,31.00,24.00,0};
T_expected expe2{0,1,33,34,34,29.00,22.00,28.00,21.00,0};
T_expected exp3{0,2,33,34,34,22.00,27.00,24.00,27.00,0};
T_expected exp5{0,8,33,34,34,21.00,23.00,36.00,20.00,0};
T_expected exp8{0,1,34,36,37,27.00,27.00,27.00,19.00,0};
T_expected exp135{1,0,37,38,38,20.00,29.00,32.00,19.00,0};
T_expected exp136{1,0,37,38,39,22.22,32.32,17.17,28.28,0};
T_expected exp149{1,0,37,37,38,22.45,30.61,23.47,23.47,0};
T_expected exp150{1,0,37,38,38,18.56,27.84,26.80,26.80,0};
T_expected exp151{1,0,37,37,38,16.67,30.21,29.17,23.96,0};
T_expected exp158{1,0,37,37,38,25.26,25.26,25.26,24.21,0};
T_expected exp160{1,0,37,37,38,23.40,27.66,21.28,27.66,0};
T_expected exp167{1,0,37,37,38,16.13,32.26,30.11,21.51,0};
T_expected exp169{1,0,37,37,38,20.65,26.09,30.43,22.83,0};
T_expected exp175{1,0,34,37,37,25.27,23.08,32.97,18.68,0};
T_expected exp176{1,0,35,37,38,15.56 ,24.44,35.56,24.44,0};
T_expected exp184{1,0,37,37,37,29.21,26.97,23.60,20.22,0};
T_expected exp191{1,0,37,37,37,15.91,35.23,26.14,22.73,0};
T_expected exp192{1,0,36,37,37,16.09,25.29,35.63,22.99,0};
T_expected exp194{1,0,33,37,37,20.93,27.91,32.56,18.60,0};
T_expected exp196{1,0,33,37,37,16.47,30.59,30.59,22.35,0};
T_expected exp208{1,0,33,37,37,27.38,29.76,23.81,19.05,0};
T_expected exp209{1,0,33,37,37,16.87,38.55,21.69,22.89,0};
T_expected exp215{1,0,35,37,37,14.63,28.05,35.37,21.95,0};
T_expected exp216{1,0,33,37,37,27.16,28.40,20.99,23.46,0};
T_expected exp218{1,0,33,37,37,26.25,25.00,25.00,23.75,0};
T_expected exp222{1,0,33,37,37,13.92,27.85,34.18,24.05,0};
T_expected exp226{1,0,33,37,37,26.92,30.77,20.51,21.79,0};
T_expected exp230{1,0,30,37,37,20.78,32.47,27.27,19.48,0};
T_expected exp233{1,0,33,37,37,15.79,21.05,36.84,26.32,0};
T_expected exp236{1,0,30,37,37,24.00,24.00,32.00,20.00,0};
T_expected exp242{2,0,33,37,37,17.57,27.03,36.49,18.92,0};
T_expected exp244{1,0,24,37,37,19.44,22.22,34.72,23.61,0};
T_expected exp249{2,0,26,37,37,19.72,33.80,28.17,18.31,0};
T_expected exp250{18,0,25,37,37,17.39,33.33,24.64,24.64,0};
T_expected exp251{51,0,13,15,33,0,33.33,39.22,27.45,0};


void check_totals(const FqInfos& fq_infos) {
	REQUIRE(fq_infos.nb_reads==100);
	REQUIRE(fq_infos.nb_bases==23423);
	REQUIRE(fq_infos.real_max_r_len==251);
	REQUIRE(fq_infos.real_max_score==40);
	REQUIRE((round(fq_infos.avg_r_len*10))/10==234.2);
	REQUIRE(fq_infos.quartiles_totPhred[0]==37);
	REQUIRE(fq_infos.quartiles_totPhred[1]==38);
	REQUIRE(fq_infos.quartiles_totPhred[2]==38);
	REQUIRE(fq_infos.quartiles_avgPhred[0]==36);
	REQUIRE(fq_infos.quartiles_avgPhred[1]==37);
	REQUIRE(fq_infos.quartiles_avgPhred[2]==38);
	REQUIRE(fq_infos.quartiles_noErrors[0]==0);
	REQUIRE(fq_infos.quartiles_noErrors[1]==0);
	REQUIRE(fq_infos.quartiles_noErrors[2]==0);
}

void check_row(const FqInfos& fq_infos, const T_expected& exp, unsigned int i) {
	// cout<<"i="<<i<<endl;
	REQUIRE(fq_infos.LFreq_nb[i]==exp.LFreq_nb);
	REQUIRE(fq_infos.EFreq_nb[i]==exp.EFreq_nb);
	REQUIRE(fq_infos.fi_quartile_phred[i-1]==exp.fi_quartile_phred);
	REQUIRE(fq_infos.sec_quartile_phred[i-1]==exp.sec_quartile_phred);
	REQUIRE(fq_infos.thi_quartile_phred[i-1]==exp.thi_quartile_phred);
	REQUIRE(round(fq_infos.A_nb[i-1]*100)==round(exp.A_pc*100));
	REQUIRE(round(fq_infos.C_nb[i-1]*100)==round(exp.C_pc*100));
	REQUIRE(round(fq_infos.G_nb[i-1]*100)==round(exp.G_pc*100));
	REQUIRE(round(fq_infos.T_nb[i-1]*100)==round(exp.T_pc*100));
	REQUIRE(round(fq_infos.N_nb[i-1]*100)==round(exp.N_pc*100));
}

TEST_CASE("Process a fastq file and returns a structure that contains information on it (number of reads, number of bases and so on") {
	FqInfos fq_infos;
	//FqBackend be("../../test/data/klebsiella_100_1.fq");
	FqBackend be("klebsiella_100_1.fq");
	be.processFile();
	fq_infos=be.getInfos();
	check_totals(fq_infos);
	for (unsigned int i=0;i<fq_infos.real_max_r_len;i++) {
		if (i==0) {
			REQUIRE(fq_infos.LFreq_nb[i]==0);
			REQUIRE(fq_infos.EFreq_nb[i]==82);
		}
		else if (i==1) check_row(fq_infos,exp1,1);
		else if (i==2) check_row(fq_infos,expe2,2);
		else if (i==3) check_row(fq_infos,exp3,3);
		else if (i==5) check_row(fq_infos,exp5,5);
		else if (i==8) check_row(fq_infos,exp8,8);
		else if (i==135) check_row(fq_infos,exp135,135);
        else if (i==136) check_row(fq_infos,exp136,136);
		else if (i==149) check_row(fq_infos,exp149,149);
		else if (i==150) check_row(fq_infos,exp150,150);
		else if (i==151) check_row(fq_infos,exp151,151);
		else if (i==158) check_row(fq_infos,exp158,158);
		else if	(i==160) check_row(fq_infos,exp160,160);
		else if (i==167) check_row(fq_infos,exp167,167);
		else if (i==169) check_row(fq_infos,exp169,169);
		else if (i==175) check_row(fq_infos,exp175,175);
		else if (i==176) check_row(fq_infos,exp176,176);
		else if (i==184) check_row(fq_infos,exp184,184);
		else if (i==191) check_row(fq_infos,exp191,191);
		else if (i==192) check_row(fq_infos,exp192,192);
		else if (i==194) check_row(fq_infos,exp194,194);
		else if (i==196) check_row(fq_infos,exp196,196);
		else if (i==208) check_row(fq_infos,exp208,208);
		else if (i==209) check_row(fq_infos,exp209,209);
		else if (i==215) check_row(fq_infos,exp215,215);
		else if (i==216) check_row(fq_infos,exp216,216);
		else if (i==218) check_row(fq_infos,exp218,218);
		else if (i==222) check_row(fq_infos,exp222,222);
		else if (i==226) check_row(fq_infos,exp226,226);
		else if (i==230) check_row(fq_infos,exp230,230);
		else if (i==233) check_row(fq_infos,exp233,233);
		else if (i==236) check_row(fq_infos,exp236,236);
		else if (i==242) check_row(fq_infos,exp242,242);
		else if (i==244) check_row(fq_infos,exp244,244);
		else if (i==249) check_row(fq_infos,exp249,249);
		else if (i==250) check_row(fq_infos,exp250,250);
		else if (i==251) check_row(fq_infos,exp251,251);
		else {
			REQUIRE(fq_infos.LFreq_nb[i]==0);
			REQUIRE(fq_infos.EFreq_nb[i]==0);
			REQUIRE(fq_infos.N_nb[i-1]==0);
		}
	}
	//std::ofstream outfile ("FqInfos.txt");
	//fq_infos.serialize(outfile);
}

TEST_CASE("Checks the case of @ and + characters in read score") {
	FqInfos fq_infos;
	//FqBackend be("../../test/data/SRR1.fq");
	FqBackend be("SRR1.fq");
	be.processFile();
	fq_infos=be.getInfos();
	REQUIRE(fq_infos.nb_reads==1);
	REQUIRE(fq_infos.nb_bases==36);
	REQUIRE((unsigned int) fq_infos.real_max_score==40);
	REQUIRE(fq_infos.tot_nb_nucl==36);
	for (unsigned p=0;p<36;p++) {
		for (unsigned int s=0;s<=(unsigned char) fq_infos.real_max_score;s++) {
			if ((p>=0 and p<=7) and s==40) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p] [s]==100.0);
			else if (p>=0 and p<=7) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p] [s]==0.0);
			else if (p>=10 and p<=17 and s==40) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p] [s]==100.0);
			else if (p>=10 and p<=17) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p] [s]==0.0);
			else if ((p>=21 and p<=22) and s==40) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p] [s]==100.0);
			else if (p>=21 and p<=22) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p] [s]==0.0);
			else if ((p>=25 and p<=27) and s==40) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p] [s]==100.0);
			else if (p>=25 and p<=27) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p] [s]==0.0);
			else if ((p>=30 and p<=31) and s==40) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p] [s]==100);
			else if (p>=30 and p<=31) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p] [s]==0.0);
			else if ((p==34) and s==40) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p] [s]==100.0);
			else if (p==34) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p] [s]==0.0);
			else if ((p==8 || p==19 ) and s==31) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p] [s]==100.0);
			else if (p==8 || p==19 ) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p] [s]==0.0);
			else if ((p==9 || p==28 || p==35) and s==10) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p] [s]==100.0);
			else if (p==9 || p==28 || p==35) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p] [s]==0.0);
			else if (p==18 and s==33) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p][s]==100.0);
			else if (p==18) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p][s]==0.0);
			else if (p==20 and s==22) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p][s]==100.0);
			else if (p==20) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p][s]==0.0);
			else if (p==23 and s==37) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p][s]==100.0);
			else if (p==23) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p][s]==0.0);
			else if (p==24 and s==28) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p][s]==100.0);
			else if (p==24) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p][s]==0.0);
			else if (p==29 and s==34) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p][s]==100.0);
			else if (p==29) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p][s]==0.0);
			else if (p==32 and s==35) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p][s]==100.0);
			else if (p==32) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p][s]==0.0);
			else if (p==33 and s==15) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p][s]==100.0);
			else if (p==33) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p][s]==0.0);

		}
	}

}

TEST_CASE("Checks the case of + characters in read identifier") {
	FqInfos fq_infos;
	FqBackend be("exemple1.fastq");
	be.processFile();
	fq_infos=be.getInfos();
	REQUIRE(fq_infos.nb_reads==2);
	REQUIRE(fq_infos.nb_bases==302);
	REQUIRE((unsigned int) fq_infos.real_max_score==36);
	REQUIRE(fq_infos.tot_nb_nucl==302);
	REQUIRE((round(fq_infos.avg_r_len*10))/10==151.0);
	REQUIRE(fq_infos.quartiles_totPhred[0]==14);
	REQUIRE(fq_infos.quartiles_totPhred[1]==36);
	REQUIRE(fq_infos.quartiles_totPhred[2]==36);
	REQUIRE(fq_infos.quartiles_avgPhred[0]==20);
	REQUIRE(fq_infos.quartiles_avgPhred[1]==20);
	REQUIRE(fq_infos.quartiles_avgPhred[2]==33);
	REQUIRE(fq_infos.quartiles_noErrors[0]==0);
	REQUIRE(fq_infos.quartiles_noErrors[1]==0);
	REQUIRE(fq_infos.quartiles_noErrors[2]==37);
	for (unsigned p=0;p<152;p++) {
		if (p==151) REQUIRE(fq_infos.LFreq_nb[p]==100.00);
		else REQUIRE(fq_infos.LFreq_nb[p]==0.00);
		if ((p==0) || (p==37)) REQUIRE(fq_infos.EFreq_nb[p]==50.00);
		else REQUIRE(fq_infos.EFreq_nb[p]==0.00);
	}
	for (unsigned p=1;p<152;p++) {
		for (unsigned int s=0;s<=(unsigned char) fq_infos.real_max_score;s++) {
			if (s==0 || s==1) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p-1][s]==0.0);
			if (s==2 && ((p>=10 &&p<=18) ||\
						 (p>=23 && p<=37) ||\
						 (p>=39 && p<=52) ||\
						 (p==55) || (p==60) || (p==62) || (p==65) || (p==78) ||\
					     (p>=82 && p<=83) || (p==94) || (p==97) || (p==107) || (p==111) ||\
						 (p==113) || (p==119) || (p==120) || (p==127) || (p==128) ||\
						 (p==130) || (p==132) || (p==130) || (p==132) || (p==144) || (p==146))) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p-1][s]==50.0);
			else if (s==2) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p-1][s]==0.0);
			if (s>=3 and s<=13) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p-1][s]==0.0);
			if (s==14 and (p==5 || p==19 ||p==66 || p==71 || p==101 || p==109 || p==112 ||\
					       p==118 ||p ==120 || p==121 || p==123 || p==125 || p==131 || p==133 ||\
						   p==136 || p==137 || p==140 || p==143 || p==147 || p==149 || p==151)) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p-1][s]==50.0);
			else if (s==14) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p-1][s]==0.0);
			if (s>=15 && s<=20) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p-1][s]==0.0);
			if (s==21 && p==98) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p-1][s]==50.0);
			else if (s==21) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p-1][s]==0.0);
			if (s>=22 && s<=26) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p-1][s]==0.0);
			if (s==27 && (p==41 || p==51 ||p==66 || p==84 || p==85 || p==95 ||\
					      p==98 || p==101 || p==105 || p==117 || p==126 || p==129 ||\
						  (p>=133 && p<=135) || (p>=137 && p<=139) ||p==142 )) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p-1][s]==50.0);
			else if (s==27) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p-1][s]==0.0);
			if (s>=28 && s<=31) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p-1][s]==0.0);
			if (s==32 && ((p>=1 && p<=4) || p==145 || p==150)) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p-1][s]==100.00);
			else if (s==32 && (p==5 || p==45 || p==50 || p==53 || (p>=61 && p<=64)|| p==74 ||\
					p==76 || p==77 || p==85 || p==86 || p==90 || p==91 || p==95 || p==96 ||\
					p==106 || p==117 || p==131 || p==135|| (p>=138 && p<=140) || p==143 ||\
					p==144 || p==148 || p==149)) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p-1][s]==50.0);
			else if (s==32) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p-1][s]==0.0);
			if (s>=33 && s<=35) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p-1][s]==0.0);
			if (s==36 && ((p>=6 && p<=9) || (p>=20 && p<=22) || p==38 || p==54 ||\
				(p>=56 && p<=59) || (p>=67 && p<=70) || p==72 || p==73 || p==75 ||\
				(p>=79 && p<=81) || (p>=87 && p<=89) || p==92 || p==93 || p==99 ||\
				p==100 || (p>=102 && p<=104) ||p==108 || p==110 || (p>=114 && p<=116) ||\
				p==122 || p==124 || p==141)) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p-1][s]==100.00);
			else if (s==36 && ((p>=10 && p<=19) || (p>=23 && p<=37) || p==39 ||\
					p==40 || (p>=42 && p<=44) || (p>=46 && p<=49) || p==52 || p==53 ||\
					p==55 || p==60 || p==61 || (p>=63 && p<=65) || p==71 || p==74 ||\
					(p>=76 && p<=78) || (p>=82 && p<=84) || p==86 || p==90 || p==91 ||\
					p==94 || p==96 || p==97 || (p>=105 && p<=107) || p==109 ||\
					(p>=111 && p<=113) || p==118 || p==119 || p==121 || p==123 ||\
					(p>=125 && p<=130) || p==132 || p==134 ||p==136 || p==142 || (p>=146 && p<=148) ||\
					p==151)) REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p-1][s]==50.0);
            else if (s==36)  {
                REQUIRE(fq_infos.nb_nucl_with_phscore_at_pos[p-1][s]==0.0);
            }
		}
	}
}



