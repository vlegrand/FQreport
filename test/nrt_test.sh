#!/bin/sh
# test script intended to be ran from exe_dir
exe_dir=$1
#test_dir=../../test
data_dir=$2
echo "test1"
pwd
#$exe_dir/fqreport -i $data_dir/klebsiella_100_1.fq>out1.txt || exit 1
#cmp out1.txt $data_dir/output_fastq_info.txt || exit 2

echo "test2"
$exe_dir/fqreport -i $data_dir/klebsiella_100_1.fq -f "test">out1.txt ||exit 3
grep -q "test" out1.txt || exit 4
cmp out1.txt $data_dir/output_fastq_info_fname.txt || exit 5

echo "test3"
cat $data_dir/klebsiella_100_1.fq|$exe_dir/fqreport>out1.txt ||exit 6
cmp out1.txt $data_dir/output_fastq_info_noname.txt || exit 7

echo "test4"
cat $data_dir/klebsiella_100_1.fq|$exe_dir/fqreport -f "test" -p 33 >out1.txt||exit 8
cmp out1.txt $data_dir/output_fastq_info_fname.txt|| exit 9

echo "test5"
cat $data_dir/klebsiella_100_1.fq|$exe_dir/fqreport -f "test" -p 33 -o out1.txt ||exit 10
cmp out1.txt $data_dir/output_fastq_info_fname.txt|| exit 11

echo "test6"
$exe_dir/fqreport -i $data_dir/klebsiella_100_1.fq -f "test" -p 33 -o out1.txt ||exit 12
cmp out1.txt $data_dir/output_fastq_info_fname.txt|| exit 13

echo "test7"
ret=`$exe_dir/fqreport -i $data_dir/klebsiella_100_1.fq -f "test" -p 34 -o out1.txt`
if [ "$?" -eq 0 ]; then exit 14;fi

echo $ret|grep -q -v "USAGE" out1.txt || exit 15
exit 0
