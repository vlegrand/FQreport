/*
 * FqInfos.cpp
 *
 *  Created on: Nov 4, 2021
 *      Author: vlegrand
 */

#include <algorithm>
#include <numeric>
#include <ostream>
#include <fstream>
#include <iostream>
#include <sstream>
#include "FqInfos.h"

using namespace std;


// TODO VL: see if passing an array rather than 3 counter gives better performances.
template void FqInfos::computeQuartiles<T_array_PhScore>(T_array_PhScore& r,unsigned long& cnt1,unsigned long& cnt2,unsigned long& cnt3);
template void FqInfos::computeQuartiles<vector<unsigned long>>(vector<unsigned long>& r,unsigned long& cnt1,unsigned long& cnt2,unsigned long& cnt3);



FqInfos::FqInfos(const string& ficname,const string& samplename) {
	filename=ficname;
	this->samplename=samplename;
	nb_reads=0;
	nb_bases=0;
	avg_r_len=0;
	real_max_r_len=0;
	real_max_score=0;
	LFreq_nb.resize(MAX_READ_LENGTH,0);
	EFreq_nb.resize(MAX_READ_LENGTH,0);
	A_nb.resize(MAX_READ_LENGTH,0);
	C_nb.resize(MAX_READ_LENGTH,0);
	G_nb.resize(MAX_READ_LENGTH,0);
	T_nb.resize(MAX_READ_LENGTH,0);
	N_nb.resize(MAX_READ_LENGTH,0);
	fi_quartile_phred.resize(MAX_READ_LENGTH,0);
	sec_quartile_phred.resize(MAX_READ_LENGTH,0);
	thi_quartile_phred.resize(MAX_READ_LENGTH,0);
	nb_nucl_per_pos.resize(MAX_READ_LENGTH,0);
	tot_nb_nucl=0;
	nb_nucl_with_phscore_at_pos.resize(MAX_READ_LENGTH);
	fill(phScoreDist_per_pos.begin(),phScoreDist_per_pos.end(),0);
	for (auto i=0; i<MAX_READ_LENGTH; i++) {
		T_array_PhScore& r=nb_nucl_with_phscore_at_pos[i];
		fill(r.begin(),r.end(),0);
	}
	fill(quartiles_totPhred.begin(),quartiles_totPhred.end(),0);
	fill(quartiles_avgPhred.begin(),quartiles_avgPhred.end(),0);
	fill(quartiles_noErrors.begin(),quartiles_noErrors.end(),0);
	fill(read_phred_scores.begin(),read_phred_scores.end(),0);
}

//! This method may be needed if one day we process long reads.
void FqInfos::resize(unsigned long m) {
	unsigned int q=m/MAX_READ_LENGTH;
	unsigned int new_size=(q+1)*MAX_READ_LENGTH;
	LFreq_nb.resize(new_size,0);
	EFreq_nb.resize(new_size,0);
	A_nb.resize(new_size,0);
	C_nb.resize(new_size,0);
	G_nb.resize(new_size,0);
	T_nb.resize(new_size,0);
	N_nb.resize(new_size,0);
	fi_quartile_phred.resize(new_size,0);
	sec_quartile_phred.resize(new_size,0);
	thi_quartile_phred.resize(new_size,0);
	nb_nucl_per_pos.resize(new_size,0);
	nb_nucl_with_phscore_at_pos.resize(new_size);
	for (unsigned long i=MAX_READ_LENGTH; i<new_size; i++) {
		T_array_PhScore& r=nb_nucl_with_phscore_at_pos[i];
		fill(r.begin(),r.end(),0);
	}
}

void FqInfos::computeACGTpercentAux(vector<double>& X_nb) {
	transform(X_nb.begin(),X_nb.end(),\
			 nb_nucl_per_pos.begin(),X_nb.begin(),\
			 [](double& n,unsigned long& tot)->double { return ((double) n)*100.0/tot; });
}

//! This method must be called after computeAllQuartiles so that the fq_infos.nb_nucl_per_pos is filled.
void FqInfos::computeACGTpercent() {
	computeACGTpercentAux(A_nb);
	computeACGTpercentAux(C_nb);
	computeACGTpercentAux(G_nb);
	computeACGTpercentAux(T_nb);
	computeACGTpercentAux(N_nb);
}

void FqInfos::computeAllQuartiles() {
	nb_nucl_per_pos.resize(real_max_r_len+1);
	for (unsigned int i=0;i<=real_max_r_len;i++) {
		T_array_PhScore& r=nb_nucl_with_phscore_at_pos[i];
		auto start=begin(r);
		auto end=std::end(r);
		auto nb_nucl_at_pos_i=accumulate(start,end,0);
		nb_nucl_per_pos[i]=nb_nucl_at_pos_i;
		transform(r.begin(), r.end(),r.begin(), [nb_nucl_at_pos_i](double& n)->double { return n*100/nb_nucl_at_pos_i; });
		tot_nb_nucl+=nb_nucl_at_pos_i;
		computeQuartiles(r,fi_quartile_phred[i],sec_quartile_phred[i],thi_quartile_phred[i]);
	}
	double tot_nb_nucl=(double) this->tot_nb_nucl;
	transform(phScoreDist_per_pos.begin(),phScoreDist_per_pos.end(),\
			  phScoreDist_per_pos.begin(),[tot_nb_nucl](double& n)->double { return n*100/tot_nb_nucl; });
	computeQuartiles(phScoreDist_per_pos,quartiles_totPhred[0],quartiles_totPhred[1],quartiles_totPhred[2]);

	double tot_nb_reads=nb_reads;
	transform(read_phred_scores.begin(),read_phred_scores.end(),\
			  read_phred_scores.begin(),[tot_nb_reads](double& n)->double { return n*100/tot_nb_reads; });
	computeQuartiles(read_phred_scores,quartiles_avgPhred[0],quartiles_avgPhred[1],quartiles_avgPhred[2]);

	transform(EFreq_nb.begin(),EFreq_nb.end(),EFreq_nb.begin(),\
			[tot_nb_reads](double& n)->double { return n*100/tot_nb_reads; });

	computeQuartiles(EFreq_nb,quartiles_noErrors[0],quartiles_noErrors[1],quartiles_noErrors[2]);
}

void FqInfos::computeAverageRLen() {
	auto sum_rlen=0;
	double div=nb_reads*1.0; // Have to use this otherwise result of the division is an integer converted into a float (so we loose all information after the dot.
	for (auto i=0; i<MAX_READ_LENGTH;i++) {
		sum_rlen+=LFreq_nb[i]*i;
	}
	avg_r_len=sum_rlen/div;
}

void FqInfos::computeAll() {
	LFreq_nb.resize(real_max_r_len+1);
	EFreq_nb.resize(real_max_r_len+1);
	A_nb.resize(real_max_r_len+1);
	C_nb.resize(real_max_r_len+1);
	G_nb.resize(real_max_r_len+1);
	T_nb.resize(real_max_r_len+1);
	N_nb.resize(real_max_r_len+1);
    fi_quartile_phred.resize(real_max_r_len+1);
	sec_quartile_phred.resize(real_max_r_len+1);
	thi_quartile_phred.resize(real_max_r_len+1);
	nb_nucl_with_phscore_at_pos.resize(real_max_r_len+1);
	nb_nucl_per_pos.resize(real_max_r_len+1);
	computeAllQuartiles();
	computeAverageRLen();
	computeACGTpercent();
	double tot_nb_reads=nb_reads;
	transform(LFreq_nb.begin(),LFreq_nb.end(),LFreq_nb.begin(),\
				[tot_nb_reads](double& n)->double { return n*100/tot_nb_reads; });
}




// writes a number whatever its type (double, unsigned int, unsigned long) to the given output stream.
template <typename T> void serializeNbr(const T& n,ostream& o) {
	string str=to_string(n);
	o.write(str.c_str(),str.length());
	o.write(&ssep,1);
}


template <typename T> void serializeVect(const vector<T>& v,ostream& o) {
	for (auto it=v.begin();it!=v.end();it++) serializeNbr(*it,o);
}

template <typename T> void deserializeVect(istringstream& is,char ssep,unsigned int vsiz, vector<T>& v) {
	string token;
	size_t siz;
	T val;
	for (unsigned int i=0;i<vsiz;i++) {
		std::getline(is,token,ssep);
		if (is_floating_point<T>::value) val=stod(token,&siz);
		else val=stoul(token,nullptr);
		v.push_back(val);
	}
}

T_array_PhScore deserializeArr(istringstream& is,char ssep) {
	T_array_PhScore a;
	string token;
	size_t siz;
	double val;
	for (unsigned int j=0;j<MAX_PHRED_SCORE+1;j++) {
		std::getline(is,token,ssep);
		val=stod(token,&siz);
		a[j]=val;
	}
	return(a);
}

array<unsigned long,3> deserializeQuartileArr(istringstream& is,char ssep) {
	array<unsigned long,3> a;
	string token;
	size_t siz;
	double val;
	for (unsigned int j=0;j<3;j++) {
		std::getline(is,token,ssep);
		val=stoul(token,&siz);
		a[j]=val;
	}
	return(a);
}

// Need that for testing and debugging purpose.
void FqInfos::serialize(ostream& o) {
	o.write(filename.c_str(),filename.length());
	o.write(&ssep,1);
	serializeNbr(nb_reads,o);
	serializeNbr(nb_bases,o);
	serializeNbr(avg_r_len,o);
	serializeNbr(real_max_r_len,o);
	serializeNbr(real_max_score,o);
	serializeVect(LFreq_nb,o);
	serializeVect(EFreq_nb,o);
	serializeVect(A_nb,o);
	serializeVect(C_nb,o);
	serializeVect(G_nb,o);
	serializeVect(T_nb,o);
	serializeVect(N_nb,o);
	serializeVect(nb_nucl_per_pos,o);
	serializeVect(fi_quartile_phred,o);
	serializeVect(sec_quartile_phred,o);
	serializeVect(thi_quartile_phred,o);

	for (auto it=nb_nucl_with_phscore_at_pos.begin();it!=nb_nucl_with_phscore_at_pos.end();it++) {
		for (auto it_arr=it->begin();it_arr!=it->end(); it_arr++) serializeNbr(*it_arr,o);
	}
	serializeNbr(tot_nb_nucl,o);
	for (auto it_arr=read_phred_scores.begin();it_arr!=read_phred_scores.end(); it_arr++) serializeNbr(*it_arr,o);
	for (auto it_arr=quartiles_totPhred.begin();it_arr!=quartiles_totPhred.end(); it_arr++) serializeNbr(*it_arr,o);
	for (auto it_arr=quartiles_avgPhred.begin();it_arr!=quartiles_avgPhred.end(); it_arr++) serializeNbr(*it_arr,o);
	for (auto it_arr=quartiles_noErrors.begin();it_arr!=quartiles_noErrors.end(); it_arr++) serializeNbr(*it_arr,o);
	for (auto it_arr=phScoreDist_per_pos.begin();it_arr!=phScoreDist_per_pos.end(); it_arr++) serializeNbr(*it_arr,o);
}



// Reads data from input stream and fill FqInfos object with it.
void FqInfos::deserialize(const string& ficname) {
	ifstream i(ficname);
	size_t siz;
	string token;
	if (i) {

	    if (!i) cout<< "error: only " << i.gcount() << " could be read";
	    else {
	    	string tmp;
	    	ostringstream ss;
	    	ss<<i.rdbuf();
	    	tmp=ss.str();
	    	istringstream is(tmp);

	    	std::getline(is,token,ssep);
	    	this->filename=token;
	    	std::getline(is,token,ssep);
	    	this->nb_reads=stoul(token,nullptr);
	    	std::getline(is,token,ssep);
	    	this->nb_bases=stoul(token,nullptr);
	    	std::getline(is,token,ssep);
	    	this->avg_r_len=stod(token,&siz);
	    	std::getline(is,token,ssep);
	    	this->real_max_r_len=(unsigned int) stoul(token,nullptr);
	    	std::getline(is,token,ssep);
	    	this->real_max_score=(unsigned int) stoul(token,nullptr);
	    	deserializeVect(is,ssep,this->real_max_r_len+1,this->LFreq_nb);
	    	deserializeVect(is,ssep,this->real_max_r_len+1,this->EFreq_nb);
	    	deserializeVect(is,ssep,this->real_max_r_len+1,this->A_nb);
	    	deserializeVect(is,ssep,this->real_max_r_len+1,this->C_nb);
	    	deserializeVect(is,ssep,this->real_max_r_len+1,this->G_nb);
	    	deserializeVect(is,ssep,this->real_max_r_len+1,this->T_nb);
	    	deserializeVect(is,ssep,this->real_max_r_len+1,this->N_nb);
	    	deserializeVect(is,ssep,this->real_max_r_len+1,this->nb_nucl_per_pos);
	    	deserializeVect(is,ssep,this->real_max_r_len+1,this->fi_quartile_phred);
	    	deserializeVect(is,ssep,this->real_max_r_len+1,this->sec_quartile_phred);
	    	deserializeVect(is,ssep,this->real_max_r_len+1,this->thi_quartile_phred);
	    	for (unsigned int i=0;i<this->real_max_r_len+1; i++) {
	    		T_array_PhScore a=deserializeArr(is,ssep);
	    		this->nb_nucl_with_phscore_at_pos.push_back(a);
	    	}
	    	std::getline(is,token,ssep);
	    	this->tot_nb_nucl=stoul(token,nullptr);
	    	this->read_phred_scores=deserializeArr(is,ssep);
	    	this->quartiles_totPhred=deserializeQuartileArr(is,ssep);
	    	this->quartiles_avgPhred=deserializeQuartileArr(is,ssep);
	    	this->quartiles_noErrors=deserializeQuartileArr(is,ssep);
	    	this->phScoreDist_per_pos=deserializeArr(is,ssep);
	    }
	    i.close();
	}

}
