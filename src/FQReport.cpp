/*

  Copyright (C) 2021  Institut Pasteur
 
  This program is part of the FQReport software.
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Veronique Legrand                                                           veronique.legrand@pasteur.fr
 
 */

#include <sys/sysctl.h>
#include <cstdlib>
#include "err.h"
#include <string.h>

#include <iostream>
#include <fstream>
#include <iomanip>
#include <map>
#include <vector>
#include <unistd.h>
#include <cmath>
#include "FqConstants.h"
#include "FqBackend.h"
#include "InfoDisplay.h"
#include "FqInfos.h"

using namespace std;

void usage(int status) {
	cout<<endl;
	cout<<"FQreport 1.1                             Copyright (C) 2021 Institut Pasteur"<<endl;
	cout<<endl;
	cout<<"USAGE: fqreport [-i <infile>] [-o <outfile>] [-f <name>] [-p <offset>] [-h]"<<endl;
	cout<<endl;
	cout<<"OPTIONS:"<<endl;
	cout<<"  -i <file>    input file in FASTQ format (default: standard input)"<<endl;
	cout<<"  -f <string>  sample name (default: file name set using option -i)"<<endl;
	cout<<"  -o <file>    output file name (default: standard output)"<<endl;
	cout<<"  -p <33|64>   Phred offset (default: 33)"<<endl;
	cout<<"  -h           print this help and exit"<<endl;
	cout<<endl;
	cout<<"EXAMPLES:"<<endl;
	cout<<"  fqreport  -i infile.fastq  -o report.txt"<<endl;
	cout<<"  fqreport  -i infile.fastq  -f sample  -p 64"<<endl;
	cout<<"  cat infile.fastq  |  fqreport  -f sample  -o report.txt"<<endl;
	cout<<"  gunzip -c infile.fastq.gz  |  fqreport"<<endl;
	cout<<endl;
	exit(status);
}


// must pass the name of the file as argument when reading from input
int main(int argc,char * argv[]) {
	int opt;
	FqInfos fq_infos;
	string sample_name="";
	string filename="";
	string outfile="";
	unsigned int phred=k_phred_33;
	while ((opt = getopt(argc,argv,"f:i:o:p:h")) != -1) {
		//cout<<opt<<","<<optarg<<endl;
	    switch(opt) {
	    	case 'f':
	    		sample_name=optarg;
	    		break;
	    	case 'i':
	    		filename=optarg;
	    		break;
	    	case 'o':
	    		outfile=optarg;
	    		break;
	    	case 'p':
	    		if ((atoi(optarg)!=k_phred_33) and (atoi(optarg)!=k_phred_64)) {
	    			cout<<"ERROR: phred offest must be either 33 either 64"<<endl;
	    			usage(-1);
	    		}
	    		phred=atoi(optarg);
	    		break;
	    	default:
	    		usage(EXIT_FAILURE);
	    		break;
	    }
	}

	FqBackend be(filename,sample_name,phred);
	if (filename=="") {
		be.processCin();
	}
	else {
		be.processFile();
	}
	fq_infos=be.getInfos();

	if (outfile=="") {
		InfoDisplay d=InfoDisplay(&cout);
		d.display(fq_infos);
	}
	else {
		ofstream out;
		out.open(outfile);
		InfoDisplay d=InfoDisplay(&out);
		d.display(fq_infos);
	}

	return EXIT_SUCCESS;
}
