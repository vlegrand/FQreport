/*
 * infoDisplay.cpp
 *
 *  Created on: Nov 2, 2021
 *      Author: vlegrand
 */

#include <iostream>
#include <iomanip>

#include "FqInfos.h"
#include "InfoDisplay.h"

const char K_COL_SEP=' ';




void InfoDisplay::display0(unsigned int w) {
	(*out)<<setw(w)<<right<<"."<<K_COL_SEP;
}

void InfoDisplay::displayPosition(unsigned int i) {
	(*out)<<setw(5)<<left<<i<<K_COL_SEP;
}

void InfoDisplay::displayFloat2D(double num,unsigned int w) {
	(*out)<<setw(w)<<right<<setprecision(2)<<num<<K_COL_SEP;
}

void InfoDisplay::displayQuartiles(const FqInfos& infos,unsigned int i) {
	(*out)<<setw(2)<<right<<infos.fi_quartile_phred[i-1]<<K_COL_SEP;
	(*out)<<setw(2)<<right<<infos.sec_quartile_phred[i-1]<<K_COL_SEP;
	(*out)<<setw(2)<<right<<infos.thi_quartile_phred[i-1]<<K_COL_SEP;
}

void InfoDisplay::displayFload1DLine(const FqInfos& infos,const T_array_PhScore& r) {
	if (r[0]==0) display0(6);
	else (*out)<<setw(6)<<setprecision(1)<<right<<r[0]<<K_COL_SEP;
	for (char j=1;j<infos.real_max_score;j++) {
		if (r[j]==0) (*out)<<setw(5)<<right<<"."<<K_COL_SEP;
		else (*out)<<setw(5)<<setprecision(1)<<right<<r[j]<<K_COL_SEP;
	}
	if (r[infos.real_max_score]==0) (*out)<<setw(5)<<right<<".";
	else (*out)<<setw(5)<<setprecision(1)<<right<<r[infos.real_max_score];
	(*out)<<endl;
}

void InfoDisplay::displayNuclPosScorePC(const FqInfos& infos,unsigned int i) {
	const T_array_PhScore& r=infos.nb_nucl_with_phscore_at_pos[i-1];
	displayFload1DLine(infos,r);
	//(*out)<<endl;
}

void InfoDisplay::displayAllPosLines(const FqInfos& infos) {
	for (unsigned int i=1;i<=infos.real_max_r_len;i++) {
		displayPosition(i);
		if (infos.LFreq_nb[i]==0) display0(6);
		else displayFloat2D(infos.LFreq_nb[i],6);
		if (infos.A_nb[i-1]==0) display0(6);
		else displayFloat2D(infos.A_nb[i-1],6);
		if (infos.C_nb[i-1]==0) display0(6);
		else displayFloat2D(infos.C_nb[i-1],6);
		if (infos.G_nb[i-1]==0) display0(6);
		else displayFloat2D(infos.G_nb[i-1],6);
		if (infos.T_nb[i-1]==0) display0(6);
		else displayFloat2D(infos.T_nb[i-1],6);
		if (infos.N_nb[i-1]==0) display0(6);
		else displayFloat2D(infos.N_nb[i-1],6);
		if (infos.EFreq_nb[i]==0) display0(6);
		else displayFloat2D(infos.EFreq_nb[i],6);
		(*out)<<K_COL_SEP;
		displayQuartiles(infos,i);
		displayNuclPosScorePC(infos,i);
	}
}

unsigned int getLineLength(const FqInfos& infos) {
	unsigned int l_length=5; // 1rst field n has width 5 char;
	l_length+=7*6; // the 7 other following fields have width 6 char.
	l_length+=3*2; // 3 following fields have  width 2 char
	l_length+=6; // Followed by a field of width 6 char
	unsigned int nb_score_fields=infos.real_max_score;
	l_length+=nb_score_fields*5; // and a various number of score fields with a width of 5 char.
	unsigned int tot_nbr_fields=nb_score_fields+13;
	unsigned int nb_spaces=tot_nbr_fields-1;
	l_length+=nb_spaces;
	return(l_length);
}


void InfoDisplay::display(const FqInfos& infos) {
	if (infos.samplename!="") (*out)<<"##File: "<<infos.samplename<<endl;
	else (*out)<<"##File: "<<infos.filename<<endl;
	(*out)<<"#no.reads(NR): "<<infos.nb_reads<<endl;
	(*out)<<"#no.bases(NB): "<<infos.nb_bases<<endl;
	(*out)<<"#avg.lgt(AL):  "<<setprecision(1)<<fixed<<right<<infos.avg_r_len<<endl;
	unsigned int l_length=getLineLength(infos);
	(*out)<<string(l_length,'-')<<endl;
	(*out)<<"n      Lfreq     pA     pC     pG     pT     pN  Efreq  Q1 Q2 Q3 Q=   0 ";
	for (unsigned int i=1; i<(unsigned int)infos.real_max_score;i++) (*out)<<setw(5)<<right<<i<<K_COL_SEP;
	(*out)<<setw(5)<<right<<(unsigned int) infos.real_max_score<<endl;
	(*out)<<"----- ------ ------ ------ ------ ------ ------ ------  -- -- -- ------";
	for (unsigned int i=1; i<=(unsigned int) infos.real_max_score;i++) (*out)<<" -----";
	(*out)<<endl;
	(*out)<<setw(5)<<left<<"0";
	(*out)<<"      .      .      .      .      .      . ";
	if (infos.EFreq_nb[0]==0) (*out)<<setw(6)<<right<<".";
	else (*out)<<setw(6)<<right<<setprecision(2)<<infos.EFreq_nb[0];
	(*out)<<"   .  .  .      . ";
	for (unsigned int i=1; i<(unsigned int) infos.real_max_score;i++) (*out)<<setw(5)<<right<<"."<<K_COL_SEP;
	(*out)<<setw(5)<<right<<"."<<endl;
	displayAllPosLines(infos);
	(*out)<<"----- ------ ------ ------ ------ ------ ------ ------  -- -- -- ------";
	for (unsigned int i=1; i<=(unsigned int) infos.real_max_score;i++) (*out)<<" -----";
	(*out)<<endl;
	(*out)<<string(56,' ')<<"Q1 Q2 Q3 Q=   0 ";
	for (unsigned int i=1; i<(unsigned int) infos.real_max_score;i++) (*out)<<setw(5)<<right<<i<<K_COL_SEP;
	(*out)<<setw(5)<<right<<(unsigned int) infos.real_max_score<<endl;
	(*out)<<string(56,' ')<<"-- -- -- ------";
	for (unsigned int i=1; i<=(unsigned int) infos.real_max_score;i++) (*out)<<" -----";
	(*out)<<endl;
	(*out)<<"all.Phred(B)                                            ";
	(*out)<<setw(2)<<infos.quartiles_totPhred[0]<<K_COL_SEP<<infos.quartiles_totPhred[1]<<K_COL_SEP<<infos.quartiles_totPhred[1]<<K_COL_SEP;
	displayFload1DLine(infos,infos.phScoreDist_per_pos);
	(*out)<<"avg.Phred(R)                                            ";
	(*out)<<setw(2)<<right<<infos.quartiles_avgPhred[0]<<K_COL_SEP;
	(*out)<<setw(2)<<right<<infos.quartiles_avgPhred[1]<<K_COL_SEP;
	(*out)<<setw(2)<<right<<infos.quartiles_avgPhred[2]<<K_COL_SEP;
	displayFload1DLine(infos,infos.read_phred_scores);
	(*out)<<"no.Errors(E)                                            ";
	(*out)<<setw(2)<<right<<infos.quartiles_noErrors[0]<<K_COL_SEP;
	(*out)<<setw(2)<<right<<infos.quartiles_noErrors[1]<<K_COL_SEP;
	(*out)<<setw(2)<<right<<infos.quartiles_noErrors[2]<<K_COL_SEP;
	(*out)<<setw(6)<<right<<"."<<K_COL_SEP;
	for (unsigned int i=1; i<(unsigned int) infos.real_max_score;i++) (*out)<<setw(5)<<right<<"."<<K_COL_SEP;
	(*out)<<setw(5)<<right<<"."<<endl;
	(*out)<<string(l_length,'-')<<endl;
}
