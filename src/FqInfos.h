/*
 * common.h
 *
 *  Created on: Nov 2, 2021
 *      Author: vlegrand
 */

#ifndef SRC_FQINFOS_H_
#define SRC_FQINFOS_H_
#include <string>
#include <vector>
#include <array>

#include "FqConstants.h"
//#include "InitAllocator.hpp"

using namespace std;

typedef array<double,MAX_PHRED_SCORE+1> T_array_PhScore;
const char ssep='|';

class FqInfos {
public:
	string filename;
	string samplename;
	unsigned long nb_reads;
	unsigned long nb_bases;
	double avg_r_len;
	unsigned int real_max_r_len;
	char real_max_score;
	vector<double> LFreq_nb; //! for each length, contains the number of reads of that length in the file being processed.
	vector<double> EFreq_nb; //! for each index i, contains the number of reads with i sequencing errors.
	vector<double> A_nb; //! for each index i, contains the total number of A at that position.
	vector<double> C_nb;
	vector<double> G_nb;
	vector<double> T_nb;
	vector<double> N_nb;
	vector<unsigned long> nb_nucl_per_pos; //! Contains the total number of nucleotides found at each position.
	vector<unsigned long> fi_quartile_phred; //! see here https://www.insee.fr/fr/metadonnees/definition/c1844 for definition of quartiles.
	vector<unsigned long> sec_quartile_phred;
	vector<unsigned long> thi_quartile_phred;
	vector<T_array_PhScore> nb_nucl_with_phscore_at_pos;
	unsigned long tot_nb_nucl;
	T_array_PhScore read_phred_scores; //! for each possible read average score, contains the numer of reads with that score.
	array<unsigned long,3> quartiles_totPhred; //! contains 1rst, 2nd and 3rd quartile of the total phread score distribution for nucleotides
	array<unsigned long,3> quartiles_avgPhred; //! contains 1rst, 2nd and 3rd quartile of the average phread score distribution for reads.
	array<unsigned long,3> quartiles_noErrors;//! contains the quartiles of the distribution of the expected number of sequencing errors per reads.
	T_array_PhScore phScoreDist_per_pos; //! contains the distribution of nucleotide phred scores whatever the nucleotide position on the read is.

	FqInfos() {};
	FqInfos(const string& ficname, const string& samplename);
	FqInfos(istream&);
	void resize(unsigned long m);
	void computeAll();
	void serialize(ostream&);
	void deserialize(const string& ficname);

private:

	void computeACGTpercent();

	void computeACGTpercentAux(vector<double>& X_nb);

	template <typename T> void computeQuartiles(T& r,unsigned long& cnt1,unsigned long& cnt2,unsigned long& cnt3) {
		double tot=0.0;
		cnt1=0;
		cnt2=0;
		cnt3=0;
		int j=0;
		auto it=r.begin();
		while (tot<75.0 && it!=r.end()) {
			if (tot<25.0) {
				tot+=*it;
				cnt1=j;
				cnt2=j;
				cnt3=j;
			}
			else if (tot<50.0) {
				tot+=*it;
				cnt2=j;
				cnt3=j;
			}
			else if (tot<75.0) {
				tot+=*it;
				cnt3=j;
			}
			j++;
			it++;
		}
	}

	void computeAllQuartiles();
	void computeAverageRLen();

};


#endif /* SRC_FQINFOS_H_ */
